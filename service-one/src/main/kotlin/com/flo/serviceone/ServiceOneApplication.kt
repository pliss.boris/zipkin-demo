package com.flo.serviceone

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ServiceOneApplication

fun main(args: Array<String>) {
    runApplication<ServiceOneApplication>(*args)
}
