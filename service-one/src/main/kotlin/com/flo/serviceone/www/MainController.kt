package com.flo.serviceone.www

import org.slf4j.LoggerFactory
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestClientException
import org.springframework.web.client.RestTemplate
import reactor.core.publisher.Flux

@RestController
class MainController(val restTemplate: RestTemplate) {

    private val log = LoggerFactory.getLogger(MainController::class.java)

    @GetMapping("demo")
    fun onDemoCall(): Flux<String> {
        val msg = restTemplate.exchange("http://localhost:8081/api/some-new-path", HttpMethod.GET, HttpEntity.EMPTY, String::class.java).body ?: throw Exception("Exception")
        log.info("Message received")
        return Flux.just(msg)
    }

    @GetMapping("exp")
    fun exp(): Flux<String>{
        return try {
            val msg = restTemplate.exchange("http://localhost:8081/api/exp", HttpMethod.GET, HttpEntity.EMPTY, String::class.java).body
            if(msg == null){
                log.info("No Resault")
                Flux.just("No result")
            }else {
                log.info("Result is", msg)
                Flux.just(msg)
            }
        }catch (ex: RestClientException){
            log.error("Errro", ex)
            Flux.just("Some Exception occurred")
        }


    }

}