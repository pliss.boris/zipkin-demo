package com.flo.servicetwo.www

import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux

@RestController
@RequestMapping("/api")
class DefaultController {

    private val log = LoggerFactory.getLogger(DefaultController::class.java)

    private val namesArray = arrayOf(
            "Boris",
            "Natalya",
            "Daniel",
            "Sheli"
    )

    @GetMapping("/some-new-path")
    fun getSomeNewPath(): Flux<String> {
        log.info("Returning array")
        return Flux.fromArray(namesArray)
    }


    @GetMapping("/exp")
    fun getSomeException(): Flux<String> {
        throw Exception("Error")
    }
}