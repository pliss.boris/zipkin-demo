package com.flo.servicetwo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ServiceTwoApplication

fun main(args: Array<String>) {
	runApplication<ServiceTwoApplication>(*args)
}
